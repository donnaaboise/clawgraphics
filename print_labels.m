function print_labels()

%
% PRINT_LABELS prints a list of labels to the graphics plot
%
% The list of labels comes from a file 'strings.dat'.   The current
% date is appended to the front of the list.

axes_curr = gca;
fig_curr = gcf;

% Append the current date to the front of the list of labels
% contained in 'strings.dat'.
label_list = date;
fid = fopen('strings.dat','r');
while 1
  str = fgets(fid);
  if (str == -1)
    break;
  end;
  % Replace underscores, since they mess up the formatting.
  label_list = strvcat(label_list,deblank(strrep(str,'_','-')));
end;
fclose(fid);

nstr = size(label_list,1);

% Add space to figure window :
label_space = nstr*20;

% axes_curr = gca;
% view_curr = view;
% view(2);

fig_rect_curr  = get(fig_curr,'Position');

pp_rect_curr = get(fig_curr,'PaperPosition');

% Create new figure window dimensions (in units of pixels)
fig_rect_new = fig_rect_curr + 1.0*[0 -label_space 0 label_space];
alpha = fig_rect_new(4)/fig_rect_curr(4);

% Reposition figure  in home screen.
set(fig_curr,'Position',fig_rect_new);

% Reposition paper
top = pp_rect_curr(2) + pp_rect_curr(4);
set(fig_curr,'Units','inches');
fig_rect_inches = get(fig_curr,'Position');
d = top - fig_rect_inches(4);
pp_rect_new = pp_rect_curr;
if (d < 0)
  pp_rect_new(2) = pp_rect_new(2) + abs(d);
  pp_rect_new(4) = pp_rect_new(4) + abs(d);
end;
set(fig_curr,'PaperPosition',pp_rect_new);


% Create strip on the bottom of the figure window so we can
% add the labels.

% Get current figure and axes dimensions
% Rect = [distance from left, distance from bottom, width, height]

% First, get axes handles
axes_rect_curr = [];
chf_hdl = get(fig_curr,'Children'); % In case we have a colorbar.
na = 0;
axes_area = [];
for i = 1:length(chf_hdl),
  if (strcmp(get(chf_hdl(i),'Type'),'axes'))
    na = na + 1;
    axes_hdl(na) = chf_hdl(i);
    p = get(axes_hdl(na),'Position');
    axes_rect_curr = [axes_rect_curr; p];
    axes_area = [axes_area p(3)*p(4)];
  end;
end;

% Get axes of largest area.  This should hopefully exclude pseudo-axes like
% colorbars and legends.

% Create new axis dimensions (in units of fractions of figure window)
ratio = label_space/fig_rect_curr(4);
axes_rect_new = axes_rect_curr;
axes_rect_new(:,2) = (axes_rect_curr(:,2) + ratio)/(1 + ratio);
axes_rect_new(:,4) = axes_rect_curr(:,4)/(1 + ratio);

% Reposition axes in figure window.
for i = 1:na,
  set(axes_hdl(i),'Position',axes_rect_new(i,:));
end;

[m,i] = max(axes_area);
axes_rect_curr = axes_rect_curr(i,:);

% now create a new axes:
text_box_rect = zeros(1,4);
text_box_rect(1) = min(axes_rect_new(:,1));
topf = 1-axes_rect_curr(2)-axes_rect_curr(4);
text_box_rect(2) = 0.5*topf;
text_box_rect(3) = max(axes_rect_new(:,3));
text_box_rect(4) = label_space/fig_rect_new(4);
axes_new = axes('Position',text_box_rect,'Box','on');
set(axes_new,'XTick',[],'YTick',[],'ZTick',[]);
set(axes_new,'Color','none');

set(axes_new,'Units','normalized');
set(axes_new,'Visible','off');

xpos = 0;
ypos = 0.85;  % Fraction of way up in label axis 'axes_new'.

hdl = [];
col2 = 0;
for i = 1:nstr,
  hdl(i) = text('Position',[xpos,ypos],'String',label_list(i,:));
  set(hdl(i),'Units','normalized');
  set(hdl(i),'FontName','FixedWidth');
  ext = get(hdl(i),'extent');
  ypos = ypos - ext(4);
end;
